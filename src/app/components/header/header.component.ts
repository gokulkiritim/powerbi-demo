import { ChangeDetectorRef, Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  currentUser: string;
  role: string;
  router: string;
  constructor(public _router: Router,private ref: ChangeDetectorRef){

    this.router = _router.url; 
    this.ref.markForCheck();
    this.currentUser = localStorage.getItem('manager_name');
    this.role = localStorage.getItem('manager_title');
    if (this.currentUser || this.role) {
      this.currentUser =
        this.currentUser.charAt(0).toUpperCase() + this.currentUser.slice(1);
      this.role = this.role.charAt(0).toUpperCase() + this.role.slice(1);
    }
}

  ngOnInit(): void {
  }
  onLogOutClick(){
    this.currentUser = null;
    this.role = null
    localStorage.clear();
    this._router.navigateByUrl("/")
  }

}
